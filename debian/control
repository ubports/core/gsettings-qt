Source: gsettings-qt
Priority: optional
Maintainer: Debian UBports Team <team+ubports@tracker.debian.org>
Uploaders:
 Mike Gabriel <sunweaver@debian.org>,
 Boyuan Yang <byang@debian.org>,
 Iceyer <iceyers@gmail.com>,
 Yanhao Mo <yanhaocs@gmail.com>,
 Debian Deepin Packaging Team <pkg-deepin-devel@lists.alioth.debian.org>,
Build-Depends:
 cmake,
 cmake-extras (>= 1.8~),
 debhelper-compat (= 13),
 libglib2.0-dev,
 pkg-config,
 qml6-module-qtqml-models,
 qml6-module-qtqml-workerscript,
 qml6-module-qtquick,
 qml6-module-qttest,
 qml-module-qtquick2,
 qml-module-qttest,
 qt6-base-dev,
 qt6-declarative-dev,
 qtbase5-dev,
 qtdeclarative5-dev (>= 5.8~),
 qtdeclarative5-dev-tools,
 libqt5qml5,
 libqt6qml6,
 xauth,
 xvfb,
Standards-Version: 4.7.0
Section: libs
Rules-Requires-Root: no
Homepage: https://gitlab.com/ubports/development/core/gsettings-qt
Vcs-Git: https://gitlab.com/ubports/development/core/gsettings-qt.git
Vcs-Browser: https://gitlab.com/ubports/development/core/gsettings-qt

Package: libgsettings-qt6-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libgio-2.0-dev,
 libgsettings-qt6-1 (= ${binary:Version}),
 ${misc:Depends},
Description: library to access GSettings from Qt6 (development files)
 Libgsettings-qt provides Qt binding to GSettings, a high-level API
 for application settings. This library can be used to access GSettings from Qt
 applications.
 .
 This package contains the development files needed to build applications using
 the GSettings Qt library for building against Qt6.

Package: libgsettings-qt-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libgio-2.0-dev,
 libgsettings-qt1 (= ${binary:Version}),
 ${misc:Depends},
Description: library to access GSettings from Qt5 (development files)
 Libgsettings-qt provides Qt binding to GSettings, a high-level API
 for application settings. This library can be used to access GSettings from Qt
 applications.
 .
 This package contains the development files needed to build applications using
 the GSettings Qt library for building against Qt5.

Package: libgsettings-qt6-1
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: library to access GSettings from Qt6 (shared libraries)
 Libgsettings-qt provides Qt binding to GSettings, a high-level API
 for application settings. This library can be used to access GSettings from Qt
 applications.
 .
 This package contains shared library files for libgsettings-qt (Qt6 build).

Package: libgsettings-qt1
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: library to access GSettings from Qt5 (shared libraries)
 Libgsettings-qt provides Qt binding to GSettings, a high-level API
 for application settings. This library can be used to access GSettings from Qt
 applications.
 .
 This package contains shared library files for libgsettings-qt (Qt5 build).

Package: qml6-module-gsettings
Architecture: any
Multi-Arch: same
Depends:
 libgsettings-qt6-1 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: QML (Qt6) Bindings for GSettings
 Libgsettings-qt provides Qt binding to GSettings, a high-level API
 for application settings. This library can be used to access GSettings from Qt
 applications.
 .
 This package provides QML bindings (Qt6) for GSettings. QML applications
 may use this library to manipulate data stored in GSettings database.

Package: qml-module-gsettings
Architecture: any
Multi-Arch: same
Depends:
 libgsettings-qt1 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Breaks: qtdeclarative5-gsettings1.0 (<< 0.2~),
Replaces: qtdeclarative5-gsettings1.0 (<< 0.2~),
Description: QML (Qt5) Bindings for GSettings
 Libgsettings-qt provides Qt binding to GSettings, a high-level API
 for application settings. This library can be used to access GSettings from Qt
 applications.
 .
 This package provides QML bindings (Qt5) for GSettings. QML applications
 may use this library to manipulate data stored in GSettings database.

Package: qml-module-gsettings1.0
Section: oldlibs
Architecture: any
Multi-Arch: same
Depends:
 qml-module-gsettings (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Breaks: qml-module-gettings1.0 (<< 0.3~),
Replaces: qml-module-gettings1.0 (<< 0.3~),
Description: QML (Qt5) Bindings for GSettings (transitional package)
 Libgsettings-qt provides Qt binding to GSettings, a high-level API
 for application settings. This library can be used to access GSettings from Qt
 applications.
 .
 This package is a transitional package to assure that qml-module-settings (Qt5
 variant, its replacement) gets installed.
